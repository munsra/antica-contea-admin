export const environment = {
  production: true,
  defaultauth: 'firebase',
  firebaseConfig: {
    apiKey: "AIzaSyA5MrlYzxC5R-G3fCY1G2s6plsxVfA-4lw",
    authDomain: "antica-contea-8ba03.firebaseapp.com",
    projectId: "antica-contea-8ba03",
    storageBucket: "antica-contea-8ba03.appspot.com",
    messagingSenderId: "14257211560",
    appId: "1:14257211560:web:2020d2df53b1792be71a9d",
    measurementId: "G-6YZK1VZV6J"
  },
  stripeServerUrl: 'https://us-central1-antica-contea-8ba03.cloudfunctions.net'
};

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  defaultauth: 'firebase',
  firebaseConfig: {
    apiKey: "AIzaSyA5MrlYzxC5R-G3fCY1G2s6plsxVfA-4lw",
    authDomain: "antica-contea-8ba03.firebaseapp.com",
    projectId: "antica-contea-8ba03",
    storageBucket: "antica-contea-8ba03.appspot.com",
    messagingSenderId: "14257211560",
    appId: "1:14257211560:web:2020d2df53b1792be71a9d",
    measurementId: "G-6YZK1VZV6J"
  },
  stripeServerUrl: 'https://us-central1-antica-contea-8ba03.cloudfunctions.net'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

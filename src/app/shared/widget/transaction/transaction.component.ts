import { Component, OnInit, Input } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/core/services/order.service';
import { StripeService } from 'src/app/core/services/stripe.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  value: number;

  @Input() transactions: Array<{
    uid?: string;
    index?: number,
    userProfile?: any,
    createdTime?: any,
    totalPrice?: string,
    status?: string,
    paymentType?: string,
  }>;

  address: any;
  selectedTransaction: any;
  addressSubscription: Subscription;
  loading = false;

  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private orderService: OrderService,
    private stripeService: StripeService
  ) { }

  ngOnInit() {
  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(data, content: any) {
    console.log("current");
    console.log(data);
    this.selectedTransaction = data;
    this.addressSubscription = this.userService.getAddressById(this.selectedTransaction.addressId).subscribe(address => {
      this.address = address;
      console.log("this.address");
      console.log(this.address);
    });
    const modal = this.modalService.open(content, { centered: true });

    modal.dismissed.subscribe(res => {
      this.addressSubscription.unsubscribe();
    });

    modal.closed.subscribe(res => {
      if (res == "TAKE") {
        this.orderService.takeOrder(this.selectedTransaction);
      }
      if (res == "COMPLETED") {
        this.orderService.completeOrder(this.selectedTransaction);
      }
      this.addressSubscription.unsubscribe();
    });
  }

  askRefund(content: any) {
    const modal = this.modalService.open(content, { centered: true });

    modal.dismissed.subscribe(res => {
      this.addressSubscription.unsubscribe();
    });

    modal.closed.subscribe(res => {
      if (res == "REFUND") {
        this.loading = true;
        console.log(this.selectedTransaction);
        const retriveCustomerBody = {
          paymentIntendId: this.selectedTransaction.stripePaymentIntent,
          refundReason: "requested_by_customer"
        }
        this.stripeService.refundPaymentIntent(retriveCustomerBody).subscribe((res: any) => {
          console.log(res);
          if(res.type){
            window.alert(res.raw.message);
          }
          if(res.cus.status == 'succeeded'){
            window.alert("Refund Completed!");
            this.orderService.refundOrder(this.selectedTransaction);
            this.modalService.dismissAll();
          }
          this.loading = false;
        });
      }
      this.addressSubscription.unsubscribe();
    });
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Post } from 'src/app/core/models/post.models';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { PostService } from 'src/app/core/services/post.service';
import * as firebase from 'firebase';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-add-new-post',
  templateUrl: './add-new-post.component.html',
  styleUrls: ['./add-new-post.component.scss']
})
export class AddNewPostComponent implements OnInit {
  file: File;
  fileUrl: any;
  post: Post;
  hasANewFile = false;

  postFormControl: FormGroup = new FormGroup({
    text: new FormControl(null, [Validators.required, Validators.minLength(2)]),
  })

  matcher = new MyErrorStateMatcher();

  constructor(
    private dialogRef: MatDialogRef<AddNewPostComponent>,
    private fbService: FirebaseService,
    private postService: PostService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit(): void {
    if(this.data){
      this.post = this.data.post;
      this.postFormControl.patchValue({
        text: this.post.text
      });
      this.fileUrl = this.post.mediaUrl;
    }
  }

  onFileChange(event){
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }

  savePost(){
    if (!this.postFormControl.valid) {
      return false;
    }
    const text = this.postFormControl.get('text').value;
    this.post = {
      uid: this.post ? this.post.uid : this.fbService.GeNewUid(),
      mediaUrl: this.post ? this.post.mediaUrl : null,
      userUid: this.post ? this.post.userUid : null,
      text: text,
      dataCreation: this.post ? this.post.dataCreation : firebase.default.firestore.Timestamp.now(),
      modDatetime: firebase.default.firestore.Timestamp.now(),
      user: this.post ? this.post.user : null,
      likes: []
    }
    if (this.hasANewFile) {
      this.setPostWithFile( this.post, this.file);
    }else{
      this.postService.setPost( this.post);
    }
    this.dialogRef.close();
  }

  setPostWithFile(post, file){
    this.fbService.UploadFile('/posts/' + post.uid, this.file).subscribe(newFile => {
      if (newFile) {
        newFile.ref.getDownloadURL().then(url => {
          post.mediaUrl = url;
          this.postService.setPost(post);
        }).catch(err => {
          console.log(err);
        });
      }
    });
  }

}

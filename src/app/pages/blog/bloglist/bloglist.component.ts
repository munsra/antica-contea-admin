import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/core/models/post.models';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { PostService } from 'src/app/core/services/post.service';
import { AddNewPostComponent } from './components/add-new-post/add-new-post.component';

@Component({
  selector: 'app-bloglist',
  templateUrl: './bloglist.component.html',
  styleUrls: ['./bloglist.component.scss']
})
export class BloglistComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  posts: Post[];
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  postsSubscription: Subscription;

  constructor(
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private firebaseService: FirebaseService,
    private postService: PostService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Blog' }, { label: 'Blog List', active: true }];
    this.postsSubscription = this.postService.getPosts().subscribe(posts => {
      this.posts = posts;
    });
  }

  addNewPostModal() {
    const dialogRef = this.dialog.open(AddNewPostComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  editPost(item: Post){
    const dialogRef = this.dialog.open(AddNewPostComponent, {
      data: {post: item}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  deleteProduct(item, content) {
    const modal = this.modalService.open(content, { windowClass: 'modal-holder' });

    modal.closed.subscribe(res => {
      console.log("closed");
      console.log(res);
      if (res == "DELETE") {
        if (item.mediaUrl) {
          this.firebaseService.DeleteFile(item.mediaUrl);
          this.postService.deletePost(item.uid).then(res => {
            this._snackBar.open("YOU'VE DELETE THIS POST!!", 'End now', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
        } else {
          this.postService.deletePost(item.uid).then(res => {
            this._snackBar.open("YOU'VE DELETE THIS POST!!", 'End now', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
        }

      }
    });
  }

}

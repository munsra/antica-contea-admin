import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbNavModule, NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";
import { UIModule } from '../../shared/ui/ui.module';

import { BlogRoutingModule } from './blog-routing.module';

import { BloglistComponent } from '../blog/bloglist/bloglist.component';
import { BloggridComponent } from '../blog/bloggrid/bloggrid.component';
import { DetailComponent } from '../blog/detail/detail.component';
import { MaterialModule } from 'src/app/material-module';
import { TranslateModule } from '@ngx-translate/core';
import { AddNewPostComponent } from './bloglist/components/add-new-post/add-new-post.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [BloglistComponent, BloggridComponent, DetailComponent, AddNewPostComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,                                                                                                                                  
        BlogRoutingModule,
        NgbNavModule,
        NgbTooltipModule,
        UIModule,
        MaterialModule,
        TranslateModule
    ]
})

export class BlogModule { }

import { Component, OnInit } from '@angular/core';
import { emailSentBarChart, monthlyEarningChart, transactions, statData } from './data';
import { ChartType } from './dashboard.model';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/core/models/user.models';
import { ProductsService } from 'src/app/core/services/products.service';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/core/services/order.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  emailSentBarChart: ChartType;
  monthlyEarningChart: ChartType;
  transactions;
  statData = [];
  currentUser: User;
  productOrder: any[];
  orders = [];
  ordersSubscription: Subscription;

  hasEmailSent = false;
  hasMonthlyEarning = false;
  hasActivity = false;
  hasTopCities = false;

  constructor(
    private authenticationService: AuthenticationService,
    private orderService: OrderService
    ) { }

  ngOnInit() {
    /**
     * Fetches the data
     */
    this.fetchData();
  }

  /**
   * Fetches the data
   */
  private fetchData() {
    this.currentUser = this.authenticationService.currentUser();
    this.emailSentBarChart = emailSentBarChart;
    this.monthlyEarningChart = monthlyEarningChart;
    this.transactions = transactions;
    this.setOrdersData();
  }

  setOrdersData(): void {
    this.ordersSubscription = this.orderService.getProductOrders().subscribe(orders => {
      this.statData = [];
      orders.sort((a, b) => (a.createdTime.seconds < b.createdTime.seconds) ? 1 : -1);
      this.orders = orders;
      this.setStatData();
    });
  }

  setStatData(): void {
    const ordersStats = {
      icon: 'bx bx-copy-alt',
      title: 'Orders',
      value: this.orders.length
    };
    let totalOrdersPrice = 0;

    this.orders.forEach(order => {
      totalOrdersPrice += order.totalPrice;
    });

    const ordersRevenue = {
      icon: 'bx bx-purchase-tag-alt',
      title: 'Revenue',
      value: '€ ' + totalOrdersPrice
    };

    this.statData.push(ordersStats);
    this.statData.push(ordersRevenue);
  }

}

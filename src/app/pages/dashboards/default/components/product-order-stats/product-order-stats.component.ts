import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/core/services/order.service';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-product-order-stats',
  templateUrl: './product-order-stats.component.html',
  styleUrls: ['./product-order-stats.component.scss']
})
export class ProductOrderStatsComponent implements OnInit, OnDestroy {

  statData = [];
  orders = [];
  ordersSubscription: Subscription;

  constructor(
    private orderService: OrderService
  ) { 
  }
  ngOnDestroy(): void {
    this.ordersSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.ordersSubscription = this.orderService.getProductOrders().subscribe(orders => {
      console.log("orders");
      console.log(orders);
      this.orders = orders;
      const ordersStats = {
        icon: 'bx bx-copy-alt',
        title: 'Orders',
        value: this.orders.length
      };
      let totalOrdersPrice = 0;

      this.orders.forEach(order => {
        totalOrdersPrice += order.totalPrice;
      });

      const ordersRevenue = {
        icon: 'bx bx-purchase-tag-alt',
        title: 'Revenue',
        value: totalOrdersPrice
      };

      this.statData.push(ordersStats);
      this.statData.push(ordersRevenue);

    });
  }

}

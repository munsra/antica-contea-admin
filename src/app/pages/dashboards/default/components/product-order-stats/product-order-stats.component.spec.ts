import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOrderStatsComponent } from './product-order-stats.component';

describe('ProductOrderStatsComponent', () => {
  let component: ProductOrderStatsComponent;
  let fixture: ComponentFixture<ProductOrderStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductOrderStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOrderStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

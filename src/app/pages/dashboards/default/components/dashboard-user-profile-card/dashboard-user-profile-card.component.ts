import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user.models';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-dashboard-user-profile-card',
  templateUrl: './dashboard-user-profile-card.component.html',
  styleUrls: ['./dashboard-user-profile-card.component.scss']
})
export class DashboardUserProfileCardComponent implements OnInit {

  //@Input() chat: Chat;
  currentUser: User;

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUser();
    console.log("currentUser");
    console.log(this.currentUser);
  }

}

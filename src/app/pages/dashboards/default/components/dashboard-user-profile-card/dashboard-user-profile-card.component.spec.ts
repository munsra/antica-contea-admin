import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardUserProfileCardComponent } from './dashboard-user-profile-card.component';

describe('DashboardUserProfileCardComponent', () => {
  let component: DashboardUserProfileCardComponent;
  let fixture: ComponentFixture<DashboardUserProfileCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardUserProfileCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardUserProfileCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

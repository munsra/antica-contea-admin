import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/core/services/order.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})

/**
 * Contacts user-list component
 */
export class UserlistComponent implements OnInit, OnDestroy {
  // bread crumb items
  breadCrumbItems: Array<{}>;

  users = [];
  orders = [];

  usersSubscription: Subscription;

  constructor(
    private orderService: OrderService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Contacts' }, { label: 'User List', active: true }];
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.usersSubscription.unsubscribe();
  }

  getUsers() {
    this.usersSubscription = this.userService.getUsers().subscribe(users => {
      this.users = users;
      this.users.forEach(user => {
        this.orderService.getOrdersByUsers(user.uid).toPromise().then((querySnapshot) => {
          user.orders = [];
          querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
            user.orders.push(doc.data());
          });
          console.log("user");
          console.log(user);
        });
      });
    })
  }

  getOrders() {
    this.orderService.getProductOrders().subscribe(orders => {
      this.orders = orders;
    });
  }
}
function flatMap(arg0: (users: any) => any): any {
  throw new Error('Function not implemented.');
}


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/core/models/user.models';
import { OrderService } from 'src/app/core/services/order.service';
import { UserService } from 'src/app/core/services/user.service';

import { revenueBarChart, statData } from './data';

import { ChartType } from './profile.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

/**
 * Contacts-profile component
 */
export class ProfileComponent implements OnInit {
  // bread crumb items
  breadCrumbItems: Array<{}>;

  revenueBarChart: ChartType;
  statData = [];

  userDetail: User;
  userDetailSubscription: Subscription;
  addressSubscription: Subscription;
  ordersSubscription: Subscription;
  orders: any[];
  addresses: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ordersService: OrderService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getUserDetail(params.id);
      this.getUserAddress(params.id);
      this.getUserOrders(params.id);
    });
  }

  /**
   * Fetches the data
   */
  private getUserDetail(userUid) {
    this.userDetailSubscription = this.userService.getUser(userUid).valueChanges().subscribe(user => {
      this.userDetail = user as User;
    });
  }

  private getUserAddress(userUid) {
    this.addressSubscription = this.userService.getAddressByUserId(userUid).subscribe(addresses => {
      this.addresses = [];
      this.addresses = addresses;
    });
  }

  private getUserOrders(userUid) {
    this.ordersSubscription = this.ordersService.getOrdersByUserId(userUid).subscribe(orders => {
      this.orders = [];
      orders.sort((a, b) => (a.createdTime.seconds < b.createdTime.seconds) ? 1 : -1);
      this.orders = orders;
      this.setStatData();
    });
  }

  setStatData(): void {
    this.statData = [];
    const ordersStats = {
      icon: 'bx bx-copy-alt',
      title: 'Orders',
      value: this.orders.length
    };
    let totalOrdersPrice = 0;

    this.orders.forEach(order => {
      totalOrdersPrice += order.totalPrice;
    });

    const ordersRevenue = {
      icon: 'bx bx-purchase-tag-alt',
      title: 'Revenue',
      value: '€ ' + totalOrdersPrice
    };

    this.statData.push(ordersStats);
    this.statData.push(ordersRevenue);
  }
}

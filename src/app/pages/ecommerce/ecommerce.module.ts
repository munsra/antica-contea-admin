import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EcommerceRoutingModule } from './ecommerce-routing.module';
import { UIModule } from '../../shared/ui/ui.module';
import { WidgetModule } from '../../shared/widget/widget.module';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgbNavModule, NgbDropdownModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { NgSelectModule } from '@ng-select/ng-select';

import { ProductsComponent } from './products/products.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { ShopsComponent } from './shops/shops.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './cart/cart.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { CustomersComponent } from './customers/customers.component';
import { OrdersComponent } from './orders/orders.component';
import { MaterialModule } from 'src/app/material-module';
import { TranslateModule } from '@ngx-translate/core';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductCategoriesComponent } from './product-categories/product-categories.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { ShippingComponent } from './shipping/shipping.component';
import { BannersComponent } from './banners/banners.component';

const config: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 100,
};

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [
    ProductsComponent, 
    ProductsListComponent,
    ProductdetailComponent, 
    ShopsComponent, 
    CheckoutComponent, 
    CartComponent, 
    AddproductComponent, 
    CustomersComponent, 
    OrdersComponent,
    ShopDetailComponent,
    ProductCategoriesComponent,
    ShippingComponent,
    BannersComponent],
  imports: [
    CommonModule,
    EcommerceRoutingModule,
    NgbNavModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgbDropdownModule,
    DropzoneModule,
    ReactiveFormsModule,
    UIModule,
    WidgetModule,
    Ng5SliderModule,
    NgSelectModule,
    MaterialModule,
    NgbPaginationModule,
    TranslateModule,
    ColorPickerModule
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: config
    }
  ]
})
export class EcommerceModule { }

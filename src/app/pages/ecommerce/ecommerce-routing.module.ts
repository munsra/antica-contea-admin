import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products/products.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { ShopsComponent } from './shops/shops.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './cart/cart.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { CustomersComponent } from './customers/customers.component';
import { OrdersComponent } from './orders/orders.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductCategoriesComponent } from './product-categories/product-categories.component';
import { ShippingComponent } from './shipping/shipping.component';
import { BannersComponent } from './banners/banners.component';

const routes: Routes = [
    {
        path: 'products',
        component: ProductsComponent
    },
    {
        path: 'products-list',
        component: ProductsListComponent
    },
    {
        path: 'product-detail/:id',
        component: ProductdetailComponent
    },
    {
        path: 'product-categories/:id',
        component: ProductCategoriesComponent
    },
    {
        path: 'shops',
        component: ShopsComponent
    },
    {
        path: 'shop-detail/:id',
        component: ShopDetailComponent
    },
    {
        path: 'checkout',
        component: CheckoutComponent
    },
    {
        path: 'cart',
        component: CartComponent
    },
    {
        path: 'add-product',
        component: AddproductComponent
    },
    {
        path: 'customers',
        component: CustomersComponent
    },
    {
        path: 'orders',
        component: OrdersComponent
    },
    {
        path: 'shipping',
        component: ShippingComponent
    },
    {
        path: 'banners',
        component: BannersComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EcommerceRoutingModule {}

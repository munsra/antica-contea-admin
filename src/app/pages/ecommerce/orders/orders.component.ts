import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/core/services/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})

/**
 * Ecommerce orders component
 */
export class OrdersComponent implements OnInit, OnDestroy {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  term: any;

  orders = [];
  ordersSubscription: Subscription;

  constructor(
    private ordersService: OrderService
  ) { }

  ngOnDestroy(): void {
    this.ordersSubscription.unsubscribe();
  }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Orders', active: true }];
    this.ordersSubscription = this.ordersService.getProductOrders().subscribe(orders => {
      orders.sort((a, b) => (a.createdTime.seconds < b.createdTime.seconds) ? 1 : -1);
      this.orders = orders;
    });
  }
}

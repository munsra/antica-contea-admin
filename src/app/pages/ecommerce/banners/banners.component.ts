import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { Banner } from 'src/app/core/models/banner.models';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { ShopService } from 'src/app/core/services/shop.service';
import * as firebase from 'firebase';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit, OnDestroy {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  file: File;
  fileUrl: any;
  downloadURL: Observable<string>;
  hasANewFile = false;
  banners: Banner[];
  selectedBanner: Banner;
  bannersSubscription: Subscription;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(
    private fbService: FirebaseService,
    private shopService: ShopService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar
  ) {

    this.bannersSubscription = this.shopService.getBanners().subscribe(banners => {
      this.banners = banners;
      console.log(this.banners);
    })
  }

  ngOnDestroy(): void {
    this.bannersSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Shops/Banners', active: true }];
  }

  openModal(data, content) {
    if(data){
      this.selectedBanner = data;
    }else{
      this.selectedBanner = {
        uid: this.fbService.GeNewUid(),
        mediaUrl: null,
        name: null,
        dataCreation: firebase.default.firestore.Timestamp.now(),
        modDatetime: firebase.default.firestore.Timestamp.now(),
        description: null,
        enable: true,
        special: false,
        shopping_groupId: null,
        special_message: null
      };
    }
    
    const modal = this.modalService.open(content, { centered: true });

    modal.closed.subscribe(res => {
      if (res == "SAVE") {

        if (this.checkBannerValidation(this.selectedBanner)) {
          this.selectedBanner.dataCreation = firebase.default.firestore.Timestamp.now();
          this.selectedBanner.modDatetime = firebase.default.firestore.Timestamp.now();
          if (this.hasANewFile) {
            this.fbService.UploadFile('/banners/' + this.selectedBanner.uid, this.file).subscribe(async newFile => {
              if (newFile) {
                await newFile.ref.getDownloadURL().then(url => {
                  this.selectedBanner.mediaUrl = url;
                  this.hasANewFile = false;
                  this.shopService.setBanner(this.selectedBanner).then(() => {
                    this._snackBar.open("YOU'VE SAVED THIS BANNER!!", 'End now', {
                      duration: 500,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                  });
                }).catch(err => {
                  console.log(err);
                });
              }
            });
          } else {
            this.shopService.setBanner(this.selectedBanner).then(() => {
              this._snackBar.open("YOU'VE SAVED THIS BANNER!!", 'End now', {
                duration: 500,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
          }

        } else {
          window.alert("Banner not valid.");
        }
      }
      else if (res == "DELETE"){
        this.shopService.deleteBanner(this.selectedBanner).then(() => {
          this._snackBar.open("YOU'VE DELETED THIS BANNER!!", 'End now', {
            duration: 500,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      }
    });
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
      this.fbService.UploadFile('/banners/' + this.selectedBanner.uid, this.file).subscribe(async newFile => {
        if (newFile) {
          await newFile.ref.getDownloadURL().then(url => {
            this.selectedBanner.mediaUrl = url;
            this.hasANewFile = false;
          }).catch(err => {
            console.log(err);
          });
        }
      });
    }
  }

  checkBannerValidation(banner: Banner): boolean {
    if (banner.mediaUrl == null) return false;
    if (banner.name == null) return false;
    return true;
  }

}

import { Component, OnInit } from '@angular/core';
import { Shop } from 'src/app/core/models/shop.models';
import { ShopService } from 'src/app/core/services/shop.service';

import { shopsData } from './data';

import { Shops } from './shops.model';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})

/**
 * Ecommerce Shops component
 */
export class ShopsComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  shopsData: Shop[];

  constructor(
    private shopService: ShopService
  ) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Shops', active: true }];

    /**
     * fetches data
     */
    this._fetchData();
  }

  /**
   * Fetches the data
   */
  private _fetchData() {
    this.shopService.getShops().subscribe(shops => {
      this.shopsData = shops;
    })
  }

}


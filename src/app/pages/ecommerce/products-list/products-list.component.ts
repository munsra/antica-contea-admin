import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/core/models/product.models';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {

   // bread crumb items
   breadCrumbItems: Array<{}>;

  public products: Product[] = [];
  productInitialVersion: Product[] = [];
  productsSubscription: Subscription;
  term: any;
  isModified: boolean = false;

  constructor(
    private productService: ProductsService
  ) {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Product List', active: true }];

    this.productsSubscription = this.productService.getProductsCollection().get().subscribe(queryProducts => {
      let products: Product[] = [];
      queryProducts.forEach(product => {
        products.push(product.data() as Product);
      })
      console.log("products");
      console.log(products);

      products.sort(function(a, b) {
        return a.sequence - b.sequence;
      });

      this.products = products;
      this.productInitialVersion = products;
    });
   }

  ngOnDestroy(): void {
    this.productsSubscription.unsubscribe();
  }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>) {
    this.isModified = true;
    moveItemInArray(this.products, event.previousIndex, event.currentIndex);
  }


  saveProducts(){
    this.products.forEach((product, index) => {
      product.sequence = index;
      this.productService.setProduct(product);
    })

    
    this.isModified = false;
  }

  clearProducts(){
    this.products = this.productInitialVersion;
    this.isModified = false;
  }

}

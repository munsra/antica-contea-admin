import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { ShippingService } from 'src/app/core/services/shipping.service';
import * as data from 'src/assets/utils/italia-province';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit, OnDestroy {

  breadCrumbItems: Array<{}>;
  places = [];
  selectedPlace: any;
  placesSubscription: Subscription;
  term: any;

  public shippingPriceForm: FormGroup;

  constructor(
    private fbService: FirebaseService,
    private shippingService: ShippingService,
    public formBuilder: FormBuilder,
    private modalService: NgbModal
  ) { 
    this.shippingPriceForm = this.formBuilder.group({
      price: ['', Validators.compose([Validators.required, Validators.min(0)])],
    });
  }
  

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Shipping', active: true }];
    this.placesSubscription = this.shippingService.getPlaces().subscribe(places => {
      this.places = places.sort((a,b) => a.regione.localeCompare(b.regione));
    })
  } 

  ngOnDestroy(): void { 
    this.placesSubscription.unsubscribe();
  }

  editPlaceShipping(index, content){
    this.selectedPlace = Object.assign({}, this.places[index]);
    const modal = this.modalService.open(content,{windowClass:'modal-holder'});

    modal.closed.subscribe(res => {
      console.log("category modal res");
      console.log(res);
      if(res == "SAVE"){
        this.selectedPlace.price = this.shippingPriceForm.value.price;
        this.shippingService.setPlace(this.selectedPlace);
      }
    });
  }
}

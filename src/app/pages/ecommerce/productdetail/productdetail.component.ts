import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Product } from 'src/app/core/models/product.models';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { ProductsService } from 'src/app/core/services/products.service';
import { productModel, productList } from '../product.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})

/**
 * Ecommerce product-detail component
 */
export class ProductdetailComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  file: File;
  fileUrl: any;
  downloadURL: Observable<string>;
  categories: any[] = [];
  selectedCategoryUid: string;
  hasANewFile = false;
  public productDetail: Product;
  public isReadOnly: boolean = true;
  public productForm: FormGroup;
  public categoryForm: FormGroup;
  productSubscriptionn: Subscription;
  categoriesSubscription: Subscription;
  color: string;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService,
    public formBuilder: FormBuilder,
    private firebaseService: FirebaseService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router,
    private modalService: NgbModal
  ) {
    
    this.route.params.subscribe(params => {
      this.productSubscriptionn = this.productService.getProduct(params.id).get().subscribe(product => {
        console.log("product");
        console.log(product.data());
        this.productDetail = product.data() as Product;
        this.selectedCategoryUid = this.productDetail.categoryId;
        this.setFormValueFromProduct(this.productDetail);
      });
    });

    this.categoriesSubscription = this.productService.getCategories().subscribe(categories => {
      const addCategory = {
        uid: "NEW",
        name: "(+) Add new category"
      }
      this.categories = categories;
      this.categories.push(addCategory);
    });

    this.productForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      type: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      alcol: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });

    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])]
    });
  }

  setFormValueFromProduct(product: Product) {
    this.productForm.controls['name'].setValue(product.name);
    this.productForm.controls['type'].setValue(product.attribute2_value?.type);
    this.productForm.controls['alcol'].setValue(product.attribute2_value?.alcol);
    this.productForm.controls['description'].setValue(product.description);
    this.color = this.productDetail.color;
  }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Product Detail', active: true }];
  }

  /**
   * onclick Image show
   * @param event image passed
   */
  imageShow(event) {
    const image = event.target.src;
    const expandImg = document.getElementById('expandedImg') as HTMLImageElement;
    expandImg.src = image;
  }

  enableEditMode() {
    this.isReadOnly = false;
  }

  saveProduct() {
    if(!this.productForm.valid){
      this._snackBar.open("Please insert all fields", 'End now', {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    this.productDetail.name = this.productForm.value.name;
    this.productDetail.color = this.color;
    this.productDetail.categoryId = this.selectedCategoryUid;
    this.productDetail.category = this.categories.filter((item) => item.uid === this.selectedCategoryUid)[0].name;
    this.productDetail.attribute2_value = {
      type: this.productForm.value.type,
      alcol: this.productForm.value.alcol
    };
    this.productDetail.description = this.productForm.value.description;
    if (this.hasANewFile) {
      this.firebaseService.UploadFile('/products/' + this.productDetail.uid, this.file).subscribe(async newFile => {
        if (newFile) {
          await newFile.ref.getDownloadURL().then(url => {
            this.productDetail.image = url;
            this.hasANewFile = false;
            this.productService.setProduct(this.productDetail).then(() => {
              this._snackBar.open("YOU'VE SAVED THIS PRODUCT!!", 'End now', {
                duration: 500,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
          }).catch(err => {
            console.log(err);
          });
        }
      });
    } else {
      this.productService.setProduct(this.productDetail).then(() => {
        this._snackBar.open("YOU'VE SAVED THIS PRODUCT!!", 'End now', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    }
    this.isReadOnly = true;
  }

  deleteProduct(content) {
    const modal = this.modalService.open(content,{windowClass:'modal-holder'});

    modal.closed.subscribe(res => {
      console.log("closed");
      console.log(res);
      if(res == "DELETE"){
        this.productDetail.name = this.productForm.value.name;
        this.productDetail.description = this.productForm.value.description;
        this.firebaseService.DeleteFile(this.productDetail.image);
        this.productService.deleteProduct(this.productDetail).then(res => {
          this._snackBar.open("YOU'VE DELETE THIS PRODUCT!!", 'End now', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.router.navigate(['/products']);
        });
      }
    });
    
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }

  addNewAttrb1() {
    const newAttrb = {
      price: "",
      value: "",
      enable: true
    };
    this.productDetail.attribute1_value.push(newAttrb);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.productDetail.attribute1_value, event.previousIndex, event.currentIndex);
  }

  disableAttrb(disableValue, index) {

    const modal = this.modalService.open(disableValue,{windowClass:'modal-holder'});

    modal.closed.subscribe(res => {
      console.log("closed");
      console.log(res);
      if(res == "DISABLE"){
        this.productDetail.attribute1_value[index].enable = false;
      }
    });
  }

  enableAttrb(index){
    this.productDetail.attribute1_value[index].enable = true;
  }

  deleteAttrb(index) {
    this.productDetail.attribute1_value.splice(index, 1);
  }

  selectedCategoryEvent(categoryUid, newCategoryModal){
    if(categoryUid == "NEW"){
      const modal = this.modalService.open(newCategoryModal,{windowClass:'modal-holder'});

      modal.closed.subscribe(res => {
        console.log("category modal res");
        console.log(res);
        if(res == "CREATE"){
          if(this.categoryForm.valid){
            const newCategory = {
              uid: this.firebaseService.GeNewUid(),
              description: this.categoryForm.value.description,
              name: this.categoryForm.value.name,
              dateCreation: new Date()
            }

            this.productService.setCategory(newCategory).then(() => {
              this._snackBar.open("YOU'VE CREATE THIS CATEGORY!!", 'End now', {
                duration: 2000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
          }
        }
      });
    }
  }
}

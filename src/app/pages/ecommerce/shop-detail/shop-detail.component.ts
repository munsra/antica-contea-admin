import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Shop } from 'src/app/core/models/shop.models';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { ShopService } from 'src/app/core/services/shop.service';

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit {
  // bread crumb items
  breadCrumbItems: Array<{}>;
  shopDetail: Shop;
  file: File;
  fileUrl: any;
  downloadURL: Observable<string>;
  hasANewFile = false;
  public isReadOnly: boolean = true;
  public productForm: FormGroup;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public formBuilder: FormBuilder,
    private firebaseService: FirebaseService,
    private _snackBar: MatSnackBar,
    private shopService: ShopService
  ) {
    this.route.params.subscribe(params => {
      this.shopService.getShop(params.id).get().subscribe(shop => {
        this.shopDetail = shop.data() as Shop;
      });
    });
   }

  ngOnInit(): void {
  }

  enableEditMode() {
    this.isReadOnly = false;
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }

  saveShop() {
    if (this.hasANewFile) {
      this.firebaseService.UploadFile('/shops/' + this.shopDetail.uid, this.file).subscribe(async newFile => {
        if (newFile) {
          await newFile.ref.getDownloadURL().then(url => {
            this.shopDetail.mediaUrl = url;
            this.hasANewFile = false;
            this.shopService.setShop(this.shopDetail).then(res => {
              this._snackBar.open("YOU'VE SAVED THIS SHOP!!", 'End now', {
                duration: 500,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
          }).catch(err => {
            console.log(err);
          });
        }
      });
    } else {
      this.shopService.setShop(this.shopDetail).then(res => {
        this._snackBar.open("YOU'VE SAVED THIS SHOP!!", 'End now', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    }
    this.isReadOnly = true;
  }

  cancel(){
    this.isReadOnly = true;
  }

}

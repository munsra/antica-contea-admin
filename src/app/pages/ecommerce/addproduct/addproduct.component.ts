import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Product } from 'src/app/core/models/product.models';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Observable } from 'rxjs';
import { ProductsService } from 'src/app/core/services/products.service';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})

/**
 * Ecommerce add-product component
 */
export class AddproductComponent implements OnInit {

  constructor(
    public formBuilder: FormBuilder,
    private http: HttpClient,
    private productService: ProductsService,
    private firebaseService: FirebaseService,
    private _snackBar: MatSnackBar,
    private router: Router) { }
  /**
   * Returns form
   */
  get form() {
    return this.productForm.controls;
  }
  newProduct: Product;
  productForm: FormGroup;
  file: File;
  fileUrl: any;
  downloadURL: Observable<string>;
  hasANewFile = false;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  // bread crumb items
  breadCrumbItems: Array<{}>;
  // Form submition
  submit: boolean;

  config: DropzoneConfigInterface = {
    // Change this to your upload POST address:
    maxFilesize: 50,
    acceptedFiles: 'image/*',
    accept: (file) => {
      this.onAccept(file);
    }
  };
  image = '';

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Add Product', active: true }];

    this.productForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
      description: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
    });
    this.submit = false;
    this.newProduct = Product.GetEmptyProduct();
  }

  onAccept(file: any) {
    this.image = file.name;
    this.file = file;

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }
  /**
   * Bootsrap validation form submit method
   */
  validSubmit() {
    this.submit = true;
    console.log("newProduct");
    console.log(this.newProduct);
    const formData = new FormData();
    formData.append('name', this.productForm.get('name').value);
    formData.append('manufacture_name', this.productForm.get('manufacture_name').value);
    formData.append('manufacture_brand', this.productForm.get('manufacture_brand').value);
    formData.append('price', this.productForm.get('price').value);
    formData.append('image', this.file, this.image);

    this.http.post<any>(`http://localhost:8000/api/products`, formData)
      .subscribe((data) => {
        // console.log('da', data);
        return data;
      });
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }

  addNewAttrb1() {
    const newAttrb = {
      price: "",
      value: ""
    };
    this.newProduct.attribute1_value.push(newAttrb);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.newProduct.attribute1_value, event.previousIndex, event.currentIndex);
  }

  deleteAttrb(index) {
    this.newProduct.attribute1_value.splice(index, 1);
  }

  saveProduct() {
    this.newProduct.name = this.productForm.value.name;
    this.newProduct.description = this.productForm.value.description;
    this.newProduct.uid = this.firebaseService.GeNewUid();
    this.newProduct.dataCreation = firebase.default.firestore.Timestamp.now();
    this.newProduct.modDatetime = firebase.default.firestore.Timestamp.now();
    this.productService.getProducts().toPromise().then(products => {
      this.newProduct.sequence = products.length + 1;

      this.firebaseService.UploadFile('/products/' + this.newProduct.uid, this.file).subscribe(async newFile => {
        if (newFile) {
          await newFile.ref.getDownloadURL().then(url => {
            this.newProduct.image = url;
            this.hasANewFile = false;
            this.productService.setProduct(this.newProduct).then(() => {
              this._snackBar.open("YOU'VE SAVED THIS PRODUCT!!", 'End now', {
                duration: 500,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.router.navigate(['/product-detail', this.newProduct.uid]);
            });
          }).catch(err => {
            console.log(err);
          });
        }
      });
    })


  }

  cancelProduct() {

  }
}

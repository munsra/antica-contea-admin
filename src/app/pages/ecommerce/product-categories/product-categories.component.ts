import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { FirebaseService } from 'src/app/core/services/firebase.service';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-product-categories',
  templateUrl: './product-categories.component.html',
  styleUrls: ['./product-categories.component.scss']
})
export class ProductCategoriesComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  categories: any[] = [];
  categoriesSubscription: Subscription;
  term: any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  public categoryForm: FormGroup;

  constructor(
    private firebaseService: FirebaseService,
    private productService: ProductsService,
    public formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private modalService: NgbModal
  ) {
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Product List', active: true }];

    this.categoriesSubscription = this.productService.getCategories().subscribe(categories => {
      this.categories = categories;
    });

    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])]
    });

  }

  ngOnInit(): void {

  }

  addNewCategory(content){
    const modal = this.modalService.open(content, { windowClass: 'modal-holder' });

    modal.closed.subscribe(res => {
      if(res == "CREATE"){
        if(this.categoryForm.valid){
          const newCategory = {
            uid: this.firebaseService.GeNewUid(),
            description: this.categoryForm.value.description,
            name: this.categoryForm.value.name,
            dateCreation: new Date()
          }

          this.productService.setCategory(newCategory).then(() => {
            this._snackBar.open("YOU'VE CREATE THIS CATEGORY!!", 'End now', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
        }
      }
    });
  }

  deleteCat(index, content) {

    const modal = this.modalService.open(content, { windowClass: 'modal-holder' });

    modal.closed.subscribe(res => {
      if (res == "DELETE") {
        const deleteUid = this.categories[index].uid;
        this.productService.deleteCategory(deleteUid).then(() => {
          this._snackBar.open("YOU'VE DELETE THIS PRODUCT!!", 'End now', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
      }
    });
  }

}

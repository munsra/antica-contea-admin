import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class StripeService {

  url = environment.stripeServerUrl;
  constructor(private http: HttpClient) { }


  addCustomer(body){
    return this.http.post(`${this.url}/addCustomer`, body);
  }

  retrieveCustomer(body){
    return this.http.post(`${this.url}/retrieveCustomer`, body);
  }

  createCardToken(body){
    return this.http.post(`${this.url}/createCardToken`, body);
  }

  addCardToCustomer(body) {
    return this.http.post(`${this.url}/addCard`, body);
  }

  getAllCard(body) {
    return this.http.post(`${this.url}/getAllCards`, body);
  }

  makePayment(body) {
    return this.http.post(`${this.url}/charge`, body)
  }

  createPaymentIntent(body){
    return this.http.post(`${this.url}/createPaymentIntent`, body);
  }

  retrivePaymentIntent(body){
    return this.http.post(`${this.url}/retrivePaymentIntent`, body);
  }

  confirmPaymentIntent(body){
    return this.http.post(`${this.url}/confirmPaymentIntent`, body);
  }

  refundPaymentIntent(body){
    return this.http.post(`${this.url}/refundPaymentIntent`, body);
  }

  retriveRefund(body){
    return this.http.post(`${this.url}/retriveRefund`, body);
  }
}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Banner } from '../models/banner.models';
import { Shop } from '../models/shop.models';
import { User } from '../models/user.models';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  private shopsCollection: AngularFirestoreCollection<Shop>;
  private shops: Observable<Shop[]>;

  private bannersCollection: AngularFirestoreCollection<Banner>;
  private banners: Observable<Banner[]>;

  constructor(public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth) {
    this.shopsCollection = afStore.collection('shops');

    this.shops = this.shopsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));

    this.bannersCollection = afStore.collection('banners');

    this.banners = this.bannersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  getShops() {
    return this.shops;
  }

  getBanners(){
    return this.banners;
  }

  getShop(uid) {
    return this.shopsCollection.doc(uid);
  }

  setShop(shop) {
    return this.shopsCollection.doc(shop.uid).set(shop);
  }

  setBanner(banner){
    return this.bannersCollection.doc(banner.uid).set(banner);
  }

  deleteBanner(banner){
    return this.bannersCollection.doc(banner.uid).delete();
  }
}

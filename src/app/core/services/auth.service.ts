import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from "@angular/fire/firestore";
import { User } from '../models/user.models';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {

    user: User;

    constructor(
        private afAuth: AngularFireAuth,
        private afStore: AngularFirestore) 
    {
        this.afAuth.onAuthStateChanged((user) => {
            if (user) {
                this.afStore.collection('admin_users').doc(user.uid).get().toPromise().then(response => {
                    const firebaseUser = response.data();
                    if(firebaseUser){
                        sessionStorage.setItem('authUser', JSON.stringify(firebaseUser));
                    }
                });
            } else {
                sessionStorage.removeItem('authUser');
            }
        });
    }

    /**
     * Returns the current user
     */
    public currentUser(): User {
        return this.getAuthenticatedUser();
    }

    /**
     * Performs the auth
     * @param email email of user
     * @param password password of user
     */
    login(email: string, password: string) {
        return this.afAuth.signInWithEmailAndPassword(email, password).then((response: any) => {
            const user = response;
            return user;
        });
    }

    /**
     * Performs the register
     * @param email email
     * @param password password
     */
    register(email: string, password: string, firstname: string, lastname: string, phone: string, ) {
        return this.afAuth.createUserWithEmailAndPassword(email, password).then((response: any) => {
            const newUser: User = {
                uid: response.user.uid,
                email: response.user.email,
                firstname: firstname,
                lastname: lastname,
                displayName: firstname + " " + lastname,
                photoURL: null,
                emailVerified: response.user.emailVerified,
                isAnonymous: false,
                isAdmin: false,
                userSettings: User.setUserSettings(response.user.uid, false),
                phone: phone,
              };
            this.afStore.collection('admin_users').doc(newUser.uid).set(newUser);
            return newUser;
        });
    }

    /**
     * Reset password
     * @param email email
     */
    resetPassword(email: string) {
        return this.afAuth.sendPasswordResetEmail(email).then((response: any) => {
            const message = response.data;
            return message;
        });
    }

    /**
     * Logout the user
     */
    logout() {
        // logout the user
        this.afAuth.signOut();
    }

    /**
     * Returns the authenticated user
     */
    getAuthenticatedUser = () => {
        if (!sessionStorage.getItem('authUser')) {
            return null;
        }
        return JSON.parse(sessionStorage.getItem('authUser'));
    }
}


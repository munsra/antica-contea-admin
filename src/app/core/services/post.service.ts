import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Post } from '../models/post.models';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private postsCollection: AngularFirestoreCollection<any>;
  private posts: Observable<any[]>;

  constructor(private afStore: AngularFirestore,) {
    this.postsCollection = afStore.collection('posts');

    this.posts = this.postsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
   }

   getPosts(){
     return this.posts;
   }

   deletePost(uid){
     return this.postsCollection.doc(uid).delete();
   }

   setPost(post: Post) {
    return this.postsCollection.doc(post.uid).set(post);
  }
}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  private placesCollection: AngularFirestoreCollection<any>;
  private places: Observable<any[]>;

  constructor(public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth) {
    this.placesCollection = afStore.collection('places');

    this.places = this.placesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  getPlaces(){
    return this.places;
  }

  setPlace(place){
    this.placesCollection.doc(place.uid).set(place);
  }
}

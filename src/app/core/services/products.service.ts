import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private productsCollection: AngularFirestoreCollection<any>;
  private products: Observable<any[]>;
  private categoriesCollection: AngularFirestoreCollection<any>;
  private categories: Observable<any[]>;

  constructor(
    private afStore: AngularFirestore
  ) { 
    this.productsCollection = afStore.collection('products');

    this.products = this.productsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    );

    this.categoriesCollection = this.afStore.collection('product_categories');

    this.categories = this.categoriesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    );
  }

  getProducts(){
    return this.products;
  }

  getProductsCollection(){
    return this.productsCollection;
  }

  getProduct(id){
    return this.productsCollection.doc(id);
  }

  getProductOrders(){
    return this.afStore.collection('product_order');
  }

  setProduct(product){
    return this.afStore.collection('products').doc(product.uid).set(product);
  }

  deleteProduct(product){
    return this.afStore.collection('products').doc(product.uid).delete();
  }

  getCategories(){
    return this.categories;
  }

  setCategory(category){
    return this.afStore.collection('product_categories').doc(category.uid).set(category);
  }

  deleteCategory(categoryUid){
    return this.afStore.collection('product_categories').doc(categoryUid).delete();
  }
}

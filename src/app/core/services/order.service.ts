import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private ordersCollection: AngularFirestoreCollection<any>;
  private orders: Observable<any[]>;

  constructor(
    private afStore: AngularFirestore,
  ) { 

    this.ordersCollection = afStore.collection('product_order');

    this.orders = this.ordersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getProductOrders() {
    return this.orders;
  }

  getOrdersByUsers(userUid){
    return this.afStore.collection('product_order', ref => ref.where('userProfileId', '==', userUid)).get();
  }

  takeOrder(order) {
    order.status = "taked";
    order.modDatetime = new Date();
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(order.orderId).set(order, options);
  }

  completeOrder(order) {
    order.status = "completed";
    order.modDatetime = new Date();
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(order.orderId).set(order, options);
  }

  refundOrder(order) {
    order.status = "refund";
    order.modDatetime = new Date();
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(order.orderId).set(order, options);
  }

  getOrdersByUserId(userId) {
    console.log("_____getOrdersByUserId=");
    return this.afStore.collection<any>('/product_order', ref => ref
      .where('userProfileId', '==', userId))
      .snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }
}

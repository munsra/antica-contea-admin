import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user.models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersCollection: AngularFirestoreCollection<User>;
  private users: Observable<User[]>;

  constructor(public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth) {
    this.usersCollection = afStore.collection('users');

    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  getUsers(){
    return this.users;
  }

  getUser(userUid){
    return this.usersCollection.doc(userUid);
  }


  //*******************************//
  //******   user address    ******//
  //*******************************//

  getAddressByUserId(userId) {
    console.log("_____getAddressByUserId=");
    return this.afStore.collection<any>('/userAddress', ref => ref
      .where('userProfileId', '==', userId))
      .snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            console.log("####get a group of countries=" + data);
            return { id, ...data };
          });
        })
      );
  }

  getAddressById(addressId: string) {
    console.log("_______getAddressById")
    return this.afStore.doc<any>('userAddress/' + addressId).valueChanges();
  }
}

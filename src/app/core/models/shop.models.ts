export class Shop {
    uid: string;
    name: string;
    description: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    phone: any;
    address: string;
    addressDetail: any;
    hours: any;
}
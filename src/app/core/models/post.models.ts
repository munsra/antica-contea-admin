export class Post {
    uid: string;
    userUid: string;
    text: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    user: any;
    comments?: any[];
    likes: any[];
}
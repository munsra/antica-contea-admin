import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

export class User {

    constructor(
        public uid: string,
        public email: string,
        public firstname: string,
        public lastname: string,
        public displayName: string,
        public photoURL: string,
        public emailVerified: boolean,
        public phone: string,
        public isAnonymous: boolean,
        public isAdmin: boolean,
        public userSettings?: UserSettings
    ){}
    

    static getUserFromFirebaseUser(user: firebase.User){
        return new User(
            user.uid,
            user.email,
            null,
            null,
            user.displayName,
            user.photoURL,
            user.emailVerified,
            user.phoneNumber,
            user.isAnonymous,
            false
        );
    }

    static setUserSettings(userUid: string, darkModeActive: boolean){
        const userSettings: UserSettings = {
            userUid: userUid,
            darkModeActive: darkModeActive
        };
        return userSettings;
    }
}

export interface UserSettings {
    userUid: string,
    darkModeActive: boolean,
}




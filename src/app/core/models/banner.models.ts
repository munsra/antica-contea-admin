export class Banner {
    uid: string;
    name: string;
    description: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    enable: boolean;
    shopping_groupId: string;
    special: boolean;
    special_message: string;
}
import * as firebase from "firebase";

export class Product {
    uid: string;
    category: string;
    categoryId: string;
    attribute1_value: any[];
    attribute1_visible: boolean;
    attribute2_value: any;
    attribute2_visible: boolean;
    description: string;
    image: string;
    name: string;
    rating: number;
    sequence: number;
    color: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;

    static GetEmptyProduct(){
        const emptyProduct: Product = {
            uid: '',
            category: '',
            categoryId: '',
            attribute1_value: [],
            attribute1_visible: true,
            attribute2_value: '',
            attribute2_visible: true,
            description: '',
            image: '',
            name: '',
            rating: 0,
            sequence: 0,
            color: '',
            dataCreation: firebase.default.firestore.Timestamp.now(),
            modDatetime: firebase.default.firestore.Timestamp.now()
        }
        return emptyProduct;
    }
}